const BicicletaSpec = require('../../models/bicicleta');
const mongoose = require('mongoose');

beforeEach(() => {
  BicicletaSpec.allBicis = [];
});

describe('Testing Bicicletas', function () {
  beforeEach(function (done) {
    const moogoDB = 'mongodb://localhost/testDB';
    mongoose.connect(moogoDB, {useNewUrlParser: true});
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'Error connection'));
    db.once('open', function () {
      console.log('Conectado a la base de datos');
      done();
    });
  });

  afterEach(function (done) {
    BicicletaSpec.deleteMany({}, function () {
      if (err) {
        console.log(errr);
      }
      done();
    });
  });

});
//
// describe('Bicicleta.all', () => {
//   it('Comienza vacia', function () {
//     expect(BicicletaSpec.allBicis.length).toBe(0);
//   });
// });
//
// describe('Bicicleta.add', () => {
//   it('Agregar bicicleta', function () {
//     expect(BicicletaSpec.allBicis.length).toBe(0);
//     const bici = new BicicletaSpec(1, 'Azul', 'Montaña');
//     BicicletaSpec.add(bici);
//     expect(BicicletaSpec.allBicis[0]).toBe(bici);
//   });
// });
// //
// describe('Bicicleta.findBYId', () => {
//   it('Debe devolver id 1', function () {
//     expect(BicicletaSpec.allBicis.length).toBe(0);
//     const bici = new BicicletaSpec(1, 'Azul', 'Montaña');
//     const bici2 = new BicicletaSpec(2, 'Rojo', 'Ciudad');
//     BicicletaSpec.add(bici);
//     BicicletaSpec.add(bici2);
//     const busqueda = BicicletaSpec.findById(1);
//     expect(busqueda.id).toBe(1);
//   });
// });