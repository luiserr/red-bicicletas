const Bicicleta = require('../../models/bicicleta');
const request  = require('request');
const server = require('../../bin/www');

beforeEach(function(){
    Bicicleta.allBicis = [];
});

describe('Bicicleta API', function(){
    describe('get Bicicletas', function(){
        it('Status 200 ', function () {
            expect(Bicicleta.allBicis.length).toBe(0);
            const bici = new Bicicleta(1, 'negra', 'deportiva')
            Bicicleta.add(bici);

            request.get('http://localhost:3000/api/bicicletas', function(err, res){
                expect(res.statusCode).toBe(200);
            });
        });
    });

    describe('post Bicicletas', function(){
        it('Status 200 ', function () {
            const bici = new Bicicleta(1, 'negra', 'deportiva')
            Bicicleta.add(bici);
            const headers = {'content-type': 'application/json'};
            const body = {
                id: 2,
                color: 'marron',
                modelo: 'montañismo'
            };
            request.post({
                url: 'http://localhost:3000/api/bicicletas/create',
                headers,
                body: JSON.stringify(body)
            }, function(err, res){
                expect(res.statusCode).toBe(200);
            });
        });
    });
});