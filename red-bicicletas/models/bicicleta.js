const moongose = require('mongoose');

const Schema = moongose.Schema;

const bicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    index: {
      type: '2dsphere',
      sparse: true
    }
  }
});

bicicletaSchema.method.toString = function () {
  return `id: ${this.id}, color: ${this.color}`;
};

bicicletaSchema.static.allBicis = function(cb){
  return this.find({}, cb);
};

bicicletaSchema.static.createInstance = function(code, color, modelo, ubicacion){
  return this({
    code,
    color,
    modelo,
    ubicacion
  });
};


var Bicicleta = function (id, color, modelo) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
}

Bicicleta.prototype.toString = function () {
  return `id: ${this.id}, color: ${this.color}`;
};

Bicicleta.allBicis = [];

Bicicleta.add = function (bici) {
  Bicicleta.allBicis.push(bici);
}

Bicicleta.findById = (idBici) => {
  const bicicleta = Bicicleta.allBicis.find(({id}) => id === idBici);
  if (bicicleta) {
    return bicicleta
  }
  throw new Error('No existe una bicicleta con este Id');
};

Bicicleta.removeById = (idBici) => {
  const bicicleta = Bicicleta.findById(idBici);
  Bicicleta.allBicis = [...Bicicleta.allBicis.filter(({id}) => id !== bicicleta.id)];

};

Bicicleta.update = (id, color, modelo) => {
  Bicicleta.allBicis = Bicicleta.allBicis.map((bicicleta) => {
    if (bicicleta.id === id) {
      return {
        ...bicicleta,
        id,
        color,
        modelo
      }
    }
    return {
      ...bicicleta
    }
  });
};

// var a = new Bicicleta('1', 'azul', 'montaña');
// var b = new Bicicleta('2', 'amarilla', 'urbana');
//
// Bicicleta.add(a);
// Bicicleta.add(b);

module.exports = bicicletaSchema;