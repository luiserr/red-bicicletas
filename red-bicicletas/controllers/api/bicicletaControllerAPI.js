const Bicicleta = require('../../models/bicicleta');

exports.list = (req, res) => {
    res.status(200).json({
        'bicicletas': Bicicleta.allBicis
    });
};

exports.create = (req, res) => {
    const {body: {id = '', color = '', modelo = ''}} = req;
    const bici = new Bicicleta(id, color, modelo);
    Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    });
};

exports.deleteBici = (req, res) => {
    const {body: {id}} = req;
    Bicicleta.removeById(id);
    res.status(204).send();
};