var Bicicleta = require('../models/bicicleta');

exports.list = function (req, res) {
    res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
};

exports.get_create = (req, res) => {
    res.render('bicicletas/create');
};

exports.create = (req, res) => {
    const {body: {id = '', color = '', modelo = ''}} = req;
    const bici = new Bicicleta(id, color, modelo);
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
};

exports.delete = (req, res) => {
    const {body: {id}} = req;
    Bicicleta.removeById(id);
    res.redirect('/bicicletas');
};

exports.get_update = (req, res) => {
    const bicicleta = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update', {bici: bicicleta});
};

exports.update = (req, res) => {
    const {body: {id = '', color = '', modelo = ''}} = req;
    Bicicleta.update(id, color, modelo);
    res.redirect('/bicicletas');
};