const  express = require('express');
const  router = express.Router();

const  bicicletasController = require('../controllers/bicicleta');

router.get('/', bicicletasController.list);
router.get('/create', bicicletasController.get_create);
router.post('/create', bicicletasController.create);
router.post('/delete/:id', bicicletasController.delete);
router.get('/update/:id', bicicletasController.get_update);
router.post('/update', bicicletasController.update);

module.exports = router;