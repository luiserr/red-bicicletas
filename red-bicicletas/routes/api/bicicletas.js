const express = require('express');

const router = express.Router();

const bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

const {list, create, deleteBici} = bicicletaController;

router.get('/', list);
router.post('/create', create);
router.delete('delete', deleteBici);

module.exports = router;